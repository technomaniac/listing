import importlib
import json

from django.apps import apps
from django.conf import settings
from django.contrib.auth import authenticate, login, logout
from django.http.response import HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from rest_framework import parsers, renderers
from rest_framework.authtoken.serializers import AuthTokenSerializer
from rest_framework.permissions import IsAdminUser
from rest_framework.response import Response
from rest_framework.views import APIView

from .authentication import XTokenAuthentication
from .models import XUser
from ..account.tasks import send_email
from ..custom.mixins import LoginRequiredMixin
from ..custom.utils import get_random_string


class ObtainAuthToken(APIView):
    throttle_classes = ()
    permission_classes = ()
    parser_classes = (parsers.FormParser, parsers.MultiPartParser, parsers.JSONParser,)
    renderer_classes = (renderers.JSONRenderer,)
    serializer_class = AuthTokenSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        return Response({'token': user.token})


obtain_auth_token = ObtainAuthToken.as_view()


class TestView(APIView):
    authentication_classes = (XTokenAuthentication,)
    permission_classes = (IsAdminUser,)

    def get(self, request):
        data = {
            'user': request.user.username,
            'created': request.user.created,
            'token': request.user.token
        }
        return Response(data)


class LoginView(APIView):
    template_name = 'xauth/login.html'

    def get(self, request):
        if request.user.is_authenticated():
            url = request.GET.get('next', '/')
            return HttpResponseRedirect(url)
        return render(request, self.template_name)

    def post(self, request, *args, **kwargs):
        user = authenticate(username=request.POST['username'], password=request.POST['password'])

        if user is not None:
            if user.is_active:
                url = request.GET.get('next', '/')
                if user.last_login is None:
                    url = '/change-password/'
                login(request, user)

                return HttpResponseRedirect(url)
            else:
                error = "The password is valid, but the account has been disabled!"

                return render(request, self.template_name, {'error': error})
        else:
            error = "The username and password were incorrect."

            return render(request, self.template_name, {'error': error})


class HomeView(LoginRequiredMixin, APIView):
    def get(self, request):
        if request.user.last_login == None:
            return HttpResponseRedirect('/change-password/')
        return HttpResponseRedirect('/listing/')


def logout_user(request):
    logout(request)
    return HttpResponseRedirect(settings.LOGIN_URL)


def reset_password(request):
    password = get_random_string(8)
    user = XUser.objects.get(pk=request.POST['id'])
    user.set_password(password)
    user.last_login = None
    user.save()

    message = 'Welcome ! {}\n\n'.format(user.name)
    message += 'Your Exclusively Listing Tool({}) password has been reset.\n'.format('http://listing.exclusively.net/')
    message += 'Login using new password - {}'.format(password)

    try:
        send_email.delay(user.username, 'Exclusively Listing Password Reset', message)
    except:
        pass

    return HttpResponse(201, content_type='application/json')


class ChangePassword(LoginRequiredMixin, APIView):
    template_name = 'xauth/change_password.html'

    def get(self, request):
        return render(request, self.template_name)

    def post(self, request):
        old_pass = request.POST['old_password']
        user = authenticate(username=request.user.username, password=old_pass)
        if user is not None:
            new_password = request.POST['new_password']
            confirm_password = request.POST['confirm_password']
            if new_password == confirm_password:
                user.set_password(new_password)
                user.save()
                login(request, user)
                url = request.GET.get('next', '/listing/')
                return HttpResponseRedirect(url)
            else:
                return render(request, template_name=self.template_name, context={'error': 'Passwords do not match.'})
        else:
            return render(request, template_name=self.template_name, context={'error': 'Wrong old password.'})


class DataTableView(LoginRequiredMixin, APIView):
    @csrf_exempt
    def post(self, request):
        params = json.loads(request.GET.get('params', "{}"))
        order_by = request.GET.get('order_by', '-id')

        limit = request.POST.get('length', 10)
        offset = request.POST.get('start', 0)
        # if request.POST.get('search[value]'):
        #     params['username__icontains'] = request.POST.get('search[value]')

        app = request.GET.get('app')
        model = request.GET.get('model')

        Model = apps.get_model(app_label=app, model_name=model.lower())
        module = importlib.import_module('apps.{}.serializers'.format(app))
        Serializer = getattr(module, '{}Serializer'.format(model))

        queryset = Model.objects.filter(**params).order_by(order_by)
        serializer = Serializer(queryset, many=True)

        response = dict()
        response['data'] = serializer.data
        response['draw'] = request.GET.get('draw', 1)
        response['recordsTotal'] = request.GET.get('recordsTotal', Model.objects.all().count())
        response['recordsFiltered'] = request.GET.get('recordsFiltered', Model.objects.filter(**params).count())

        return Response(response)
