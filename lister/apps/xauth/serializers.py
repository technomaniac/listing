from rest_framework import serializers

from .models import XUser


class XUserSerializer(serializers.ModelSerializer):
    action = serializers.ReadOnlyField(source='id', read_only=True)

    class Meta:
        model = XUser
        fields = ('id', 'created', 'username', 'name', 'role_title', 'contact', 'action')
