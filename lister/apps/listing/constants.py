IMAGE_AVAILABILITY = (('Real', 'Real'), ('Virtual', 'Virtual'))

LISTING_STATUS = (
    # Studio Status
    ('Yet to Start', 'Yet to Start'),
    ('Inward', 'Inward'),
    ('QC Done', 'QC Done'),
    # ('Partially Rejected', 'Partially Rejected'),
    # ('Completely Rejected', 'Completely Rejected'),
    ('Shoot Done', 'Shoot Done'),
    ('Foldering Done', 'Foldering Done'),
    ('Editing Done', 'Editing Done'),
    # Listing
    ('Content Sheet Ready', 'Content Sheet Ready'),  # listing content - only sheet
    ('Upload Format Ready', 'Upload Format Ready'),  # listing ops - only sheet
    ('Upload Done', 'Upload Done'),
)

STUDIO_MANAGER_LISTING_STATUS = ['Yet to Start', 'Inward', 'QC Done', 'Shoot Done', 'Foldering Done', 'Editing Done']

LISTING_STATUS_PIPELINE = [i[0] for i in LISTING_STATUS]

CONTENT_MANDATORY_SHEETS = ['welcome', 'image mapping tool', 'image guide-st', 'validation log']
CONTENT_SHEETS_COLUMNS = ['SKU Code', 'Product Name', 'Description', 'Colour', 'Inclusion', ['Material', 'Fabric'],
                          'Product Actual Size']
