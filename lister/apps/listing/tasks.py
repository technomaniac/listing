import time
from datetime import datetime, timedelta
from itertools import chain

from celery.task.base import task
from django.conf import settings

from .models import ListingRequest
from ..account.models import StudioManager
from ..account.tasks import send_mail_attachment, send_email
from ..xauth.models import XUser


@task
def send_complete_rejection_alert(listing_id, user_id):
    listing = ListingRequest.objects.get(pk=listing_id)
    user = XUser.objects.get(pk=user_id)

    send_to = [listing.account_manager.user.username, user.username]
    subject = 'Exclusively Listing : LOT {} Rejected'.format(listing.lot_name)
    files = [settings.MEDIA_ROOT + str(listing.studio_rejected_sheet), settings.MEDIA_ROOT + str(listing.content_sheet)]
    text = 'Listing Request - Lot {} is completely rejected by {} - {}.\nReason - {}'.format(listing.lot_name,
                                                                                             user.role_title,
                                                                                             user.name,
                                                                                             listing.remark)

    send_mail_attachment(send_to, subject, text, files)


@task
def remove_complete_listing_requests():
    ListingRequest.objects.filter(listing_status__in=['Upload Done', 'Complete Rejected'],
                                  is_active=True).update(is_active=False)


@task
def inward_reminder(reminding_days_virtual=1, reminding_days_real=3):
    last_updated_virtual = datetime.now() - timedelta(days=reminding_days_virtual)
    timestamp_virtual = int(time.mktime(last_updated_virtual.timetuple()))

    last_updated_real = datetime.now() - timedelta(days=reminding_days_real)
    timestamp_real = int(time.mktime(last_updated_real.timetuple()))

    virtual_listings = ListingRequest.active.filter(listing_status='Yet to Start',
                                                    image_availability='Virtual',
                                                    created_on__lte=timestamp_virtual)

    real_listings = ListingRequest.active.filter(listing_status='Yet to Start',
                                                 image_availability='Real',
                                                 created_on__lte=timestamp_real)

    listings = list(chain(real_listings, virtual_listings))

    if listings:

        studio_managers = StudioManager.active.all().select_related('user')
        studio_managers_emails = [sm.user.username for sm in studio_managers]

        for listing in listings:
            send_to = [listing.account_manager.user.username] + studio_managers_emails
            subject = 'Listing : LOT {} Inward Reminder'.format(listing.lot_name)
            msg = 'Studio Process for lot {} has not been initiated. Please start the process as soon as possible.'.format(
                listing.lot_name)

            send_email.delay(send_to, subject, msg)
