import json

from django.db.transaction import atomic
from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from rest_condition import Or
from rest_framework.permissions import IsAdminUser
from rest_framework.response import Response
from rest_framework.views import APIView

from .tasks import send_complete_rejection_alert
from .constants import LISTING_STATUS_PIPELINE, STUDIO_MANAGER_LISTING_STATUS
from .models import ListingRequest
from .utils import validate_content_sheet
from ..account.models import AccountManager
from ..account.permissions import IsAccountManager
from ..custom.mixins import LoginRequiredMixin
from ..inventory.models import SDSeller, ExSeller
import time


class ListingDetailView(LoginRequiredMixin, APIView):
    def delete(self, request, pk):
        ListingRequest.objects.filter(pk=pk).delete()
        if request.is_ajax():
            return Response(status=201)
        return HttpResponseRedirect('/listing/')

    def post(self, request, pk):

        listing = ListingRequest.objects.get(pk=pk)
        listing.updated_by = request.user

        if 'am_priority' in request.data:

            max_am_priority = ListingRequest.get_max_am_priority(listing.account_manager)

            if int(request.data['am_priority']) > max_am_priority:
                am_priority = max_am_priority
            elif int(request.data['am_priority']) < 1:
                am_priority = 1
            else:
                am_priority = int(request.data['am_priority'])

            ListingRequest.objects.filter(am_priority=am_priority,
                                          account_manager=listing.account_manager,
                                          is_active=True).update(am_priority=listing.am_priority)

            listing.am_priority = am_priority

        if request.data.get('listing_priority'):
            max_listing_priority = ListingRequest.get_max_listing_priority()

            if int(request.data['listing_priority']) > max_listing_priority:
                listing_priority = max_listing_priority
            elif int(request.data['listing_priority']) < 1:
                listing_priority = 1
            else:
                listing_priority = int(request.data['listing_priority'])

            ListingRequest.objects.filter(listing_priority=listing_priority,
                                          is_active=True).update(listing_priority=listing.listing_priority)

            listing.listing_priority = listing_priority

        if request.data.get('listing_resource_allocated'):
            listing.listing_resource_allocated_id = int(request.data['listing_resource_allocated'])

        if request.FILES.get('studio_sku_content_sheet'):
            listing.studio_sku_content_sheet = request.FILES['studio_sku_content_sheet']
            listing.studio_inward_confirmation = True
            listing.listing_status = 'Inward'

        if request.FILES.get('listing_content_sheet'):
            listing.listing_content_sheet = request.FILES['listing_content_sheet']
            listing.listing_status = 'Content Sheet Ready'

        if request.POST.get('studio_image_type'):
            if request.POST['studio_image_type'] == 'path':
                listing.studio_images = 'PATH|{}'.format(request.POST['studio_image_path'])
            elif request.POST['studio_image_type'] == 'file':
                listing.studio_images = request.FILES['studio_image_file']
            listing.listing_status = 'Editing Done'

        if request.data.get('listing_status'):
            listing.listing_status = request.data['listing_status']

        if request.FILES.get('studio_rejected_sheet'):
            listing.studio_rejected_sheet = request.FILES['studio_rejected_sheet']
            listing.remark = request.POST['remark']
            history = {'status': 'Partial Rejected',
                       'updated_by': request.user.pk,
                       'timestamp': int(time.time()),
                       'remark': listing.remark}

            if request.POST['listing_status']:
                listing.listing_status = 'Complete Rejected'
                send_complete_rejection_alert.delay(listing_id=listing.pk, user_id=request.user.pk)
                history['status'] = listing.listing_status

            listing.listing_status_history = [history] + listing.listing_status_history

        if request.FILES.get('listing_upload_sheet'):
            listing.listing_upload_sheet = request.FILES['listing_upload_sheet']
            listing.listing_status = 'Upload Format Ready'

        listing.save()

        if request.is_ajax():
            return Response(status=201)
        return HttpResponseRedirect('/listing/')


class ListingView(LoginRequiredMixin, APIView):
    def get(self, request):
        context = dict()
        context['is_active'] = True

        if request.user.role == 'account_manager':
            context['account_manager_id'] = request.user.account_manager.pk

        if request.user.role == 'listing_content':
            context['listing_status'] = 'Editing Done'

        if request.user.role == 'listing_operations':
            context['listing_status__in'] = ['Content Sheet Ready', 'Upload Format Ready', 'Upload Done']

        if request.user.role == 'studio_manager':
            context['listing_status__in'] = STUDIO_MANAGER_LISTING_STATUS

        template_name = 'listing/{}_listings.html'.format(request.user.role)
        return render(request, template_name, context={'params': json.dumps(context)})


class CreateListingView(LoginRequiredMixin, APIView):
    permission_classes = (Or(IsAdminUser, IsAccountManager),)

    template_name = 'listing/create_listing.html'

    def get(self, request):
        context = dict()
        context['sd_sellers'] = SDSeller.objects.all().order_by('name')
        context['ex_sellers'] = ExSeller.objects.all().order_by('name')
        if request.user.role == 'admin':
            context['account_managers'] = AccountManager.objects.all()
        return render(request, self.template_name, context=context)

    @atomic
    def post(self, request):
        data = dict()
        data['updated_by'] = request.user
        data['sd_seller'] = SDSeller.objects.get(pk=request.POST['sd_seller'])

        if data['sd_seller'].is_vtipl and request.POST.get('ex_seller'):
            data['ex_seller_id'] = request.POST['ex_seller']

        if request.POST.get('pogs'):
            data['pogs'] = int(request.POST.get('pogs', 0))

        if 'account_manager' in request.POST:
            data['account_manager_id'] = request.POST['account_manager']
        else:
            data['account_manager_id'] = request.user.account_manager.pk
        data['image_availability'] = request.POST['image_availability']

        # content_sheet_errors = validate_content_sheet(request.FILES['content_sheet'])
        # if content_sheet_errors:
        #     return render(request, self.template_name, context={'content_sheet_errors': content_sheet_errors})

        data['content_sheet'] = request.FILES['content_sheet']
        data['size_chart'] = request.FILES['size_chart']

        if data['image_availability'] == 'Virtual':
            data['images'] = request.FILES['image_zip']

        ListingRequest.objects.create(**data)
        return HttpResponseRedirect('/listing/')


class ListingActionView(LoginRequiredMixin, APIView):
    template_name = 'listing/action_form.html'

    def get(self, request, pk):
        listing = ListingRequest.objects.get(pk=pk)
        action = request.GET['action']

        if action == 'revert':
            status_index = LISTING_STATUS_PIPELINE.index(listing.listing_status)
            if status_index > 0:
                listing.listing_status = LISTING_STATUS_PIPELINE[status_index - 1]
                listing.updated_by = request.user
                listing.save()
            return Response(status=201)

        return render(request, template_name=self.template_name, context={'listing': listing, 'action': action})
