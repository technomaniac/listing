from datetime import datetime

import xlrd

from .constants import CONTENT_MANDATORY_SHEETS, CONTENT_SHEETS_COLUMNS
import re


def content_sheet_upload_path(instance, filename):
    ext = filename.split('.')
    file = 'CONTENT_SHEET_{}.{}'.format(datetime.now().strftime('%d-%m-%Y_%H-%M-%S'), ext[-1])
    return 'listing/{}'.format(file)


def rejected_sheet_upload_path(instance, filename):
    ext = filename.split('.')
    file = 'REJECTED_SHEET_{}.{}'.format(datetime.now().strftime('%d-%m-%Y_%H-%M-%S'), ext[-1])
    return 'listing/{}'.format(file)


def sku_content_sheet_upload_path(instance, filename):
    ext = filename.split('.')
    file = 'SKU_CONTENT_SHEET_{}.{}'.format(datetime.now().strftime('%d-%m-%Y_%H-%M-%S'), ext[-1])
    return 'listing/{}'.format(file)


def listing_content_sheet_upload_path(instance, filename):
    ext = filename.split('.')
    file = 'LISTING_CONTENT_SHEET_{}.{}'.format(datetime.now().strftime('%d-%m-%Y_%H-%M-%S'), ext[-1])
    return 'listing/{}'.format(file)


def upload_sheet_upload_path(instance, filename):
    ext = filename.split('.')
    file = 'UPLOAD_SHEET_{}.{}'.format(datetime.now().strftime('%d-%m-%Y_%H-%M-%S'), ext[-1])
    return 'listing/{}'.format(file)


def image_upload_path(instance, filename):
    ext = filename.split('.')
    file = 'IMAGES_{}.{}'.format(datetime.now().strftime('%d-%m-%Y_%H-%M-%S'), ext[-1])
    return 'listing/{}'.format(file)


def studio_image_upload_path(instance, filename):
    ext = filename.split('.')
    file = 'STUDIO_IMAGES_{}.{}'.format(datetime.now().strftime('%d-%m-%Y_%H-%M-%S'), ext[-1])
    return 'listing/{}'.format(file)


def size_chart_upload_path(instance, filename):
    ext = filename.split('.')
    file = 'SIZE_CHART_{}.{}'.format(datetime.now().strftime('%d-%m-%Y_%H-%M-%S'), ext[-1])
    return 'listing/{}'.format(file)


def validate_content_sheet(content_sheet):
    errors = []
    workbook = xlrd.open_workbook(file_contents=content_sheet.read())
    sheets = workbook.sheets()
    mandatory_sheets = [i for i in CONTENT_MANDATORY_SHEETS if i not in [s.name.lower() for s in sheets[:4]]]

    if mandatory_sheets:
        errors.append('{} sheets not found in Content Sheet'.format(', '.join(mandatory_sheets)))
    # print (len(sheets))
    # if len(sheets) > 7:
    #     errors.append('Only single bucket data is allowed in Content Sheet')

    # try:
    #     bucket_sheet = sheets[4]
    #     headers = [bucket_sheet.cell(1, col_index).value for col_index in xrange(bucket_sheet.ncols)]
    #     mandatory_columns = []
    #     for col in CONTENT_SHEETS_COLUMNS:
    #         if type(col) is list:
    #             found = False
    #             for c in col:
    #                 if c in headers:
    #                     found = True
    #             if not found:
    #                 mandatory_columns.append('/'.join(col))
    #         else:
    #             if col not in headers:
    #                 mandatory_columns.append(col)
    #
    #     # mandatory_columns = [c for c in CONTENT_SHEETS_COLUMNS if c not in headers]
    #     if mandatory_columns:
    #         errors.append('{} headers not found in Content Bucket Sheet'.format(', '.join(mandatory_columns)))
    # except IndexError:
    #     errors.append('Invalid Content Sheet')
    # except Exception as e:
    #     errors.append('Invalid Content Sheet Error: {}'.format(e.message))

    return errors
