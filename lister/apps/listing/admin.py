from django.contrib import admin

# Register your models here.
from .models import *


class ListingRequestAdmin(admin.ModelAdmin):
    list_display = [f.name for f in ListingRequest._meta.get_fields()]
    search_fields = ['lot_name']
    list_filter = ['account_manager', 'listing_status']


admin.site.register(ListingRequest, ListingRequestAdmin)
