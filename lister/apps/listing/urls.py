from django.conf.urls import patterns, url

from .views import *

urlpatterns = patterns('apps.listing.views',
                       url(r'^$', ListingView.as_view()),
                       url(r'^create/$', CreateListingView.as_view()),
                       url(r'^/$', CreateListingView.as_view()),
                       url(r'^(\d+)/$', ListingDetailView.as_view()),
                       url(r'^(\d+)/action/$', ListingActionView.as_view()),
                       )
