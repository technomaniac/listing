from rest_framework import serializers

from .models import ListingRequest


class ListingRequestSerializer(serializers.ModelSerializer):
    account_manager = serializers.ReadOnlyField(source='account_manager.name', read_only=True)
    listing_resource_allocated = serializers.ReadOnlyField(source='listing_resource_allocated.name', read_only=True)
    action = serializers.ReadOnlyField(source='id', read_only=True)
    exl_seller = serializers.ReadOnlyField(source='ex_seller.name', read_only=True)

    class Meta:
        model = ListingRequest
        fields = (
            'id', 'lot_name', 'account_manager', 'pogs', 'am_priority', 'image_availability', 'images', 'content_sheet',
            'studio_rejected_sheet', 'studio_sku_content_sheet', 'size_chart', 'studio_image_path',
            'listing_priority', 'listing_resource_allocated', 'listing_content_sheet', 'listing_status',
            'submission_date', 'remark', 'exl_seller', 'action')
