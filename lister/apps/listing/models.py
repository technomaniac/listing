from __future__ import unicode_literals

import time
from datetime import datetime

from django.db import models
from django.db.models.aggregates import Max
from django_pgjson.fields import JsonBField

from .constants import LISTING_STATUS, IMAGE_AVAILABILITY
from .utils import content_sheet_upload_path, image_upload_path, size_chart_upload_path, rejected_sheet_upload_path, \
    studio_image_upload_path, sku_content_sheet_upload_path, listing_content_sheet_upload_path, upload_sheet_upload_path
from ..base.models import GenericBaseModel
from ..custom.utils import convert_timestamp_to_date
from ..xauth.models import XUser


class ListingRequest(GenericBaseModel):
    # account manager related fields
    lot_name = models.CharField(max_length=100)
    account_manager = models.ForeignKey('account.AccountManager', related_name='listing')
    pogs = models.IntegerField()
    sd_seller = models.ForeignKey('inventory.SDSeller')
    ex_seller = models.ForeignKey('inventory.ExSeller', null=True, blank=True)
    am_priority = models.IntegerField(default=1)
    # am_priority_reason = models.CharField(choices=AM_PRIORITY_REASONS, max_length=50)
    image_availability = models.CharField(choices=IMAGE_AVAILABILITY, max_length=20, default='Virtual')
    images = models.FileField(upload_to=image_upload_path, null=True, blank=True)
    content_sheet = models.FileField(upload_to=content_sheet_upload_path, null=True, blank=True)
    size_chart = models.FileField(upload_to=size_chart_upload_path, null=True, blank=True)
    # studio related fields
    studio_rejected_sheet = models.FileField(upload_to=rejected_sheet_upload_path, null=True, blank=True)
    studio_sku_content_sheet = models.FileField(upload_to=sku_content_sheet_upload_path, null=True, blank=True)
    studio_images = models.FileField(upload_to=studio_image_upload_path, null=True, blank=True)
    # listing related fields
    listing_priority = models.IntegerField(default=1)
    listing_resource_allocated = models.ForeignKey('account.ListingContent', null=True, blank=True)
    listing_status = models.CharField(choices=LISTING_STATUS, max_length=30, default='Yet to Start')
    listing_status_history = JsonBField(default=[], null=True, blank=True)
    listing_content_sheet = models.FileField(upload_to=listing_content_sheet_upload_path, null=True, blank=True)
    listing_upload_sheet = models.FileField(upload_to=upload_sheet_upload_path, null=True, blank=True)
    remark = models.TextField(null=True)

    def __unicode__(self):
        return '{}'.format(self.lot_name)

    def save(self, *args, **kwargs):
        if not self.lot_name:
            date = datetime.now()
            self.lot_name = '{}'.format(self.sd_seller.code)
            if self.ex_seller:
                self.lot_name += '_{}'.format(self.ex_seller.code)
            self.lot_name += '_{}_{}'.format(date.strftime('%b%d'), self.pogs)

        if not self.pk:
            self.listing_priority = self.get_max_listing_priority() + 1
            self.am_priority = self.get_max_am_priority(self.account_manager) + 1

            history = {'status': self.listing_status,
                       'updated_by': self.updated_by.pk,
                       'timestamp': int(time.time())}

            if self.remark:
                history['remark'] = self.remark
            self.listing_status_history = [history]

        else:
            if self.listing_status_history[0]['status'] != self.listing_status:
                history = {'status': self.listing_status,
                           'updated_by': self.updated_by.pk,
                           'timestamp': int(time.time())}

                self.listing_status_history = [history] + self.listing_status_history

        super(ListingRequest, self).save(*args, **kwargs)

    @classmethod
    def get_max_am_priority(cls, account_manager):
        am_priority = cls.objects.filter(account_manager=account_manager,
                                         is_active=True).aggregate(Max('am_priority'))['am_priority__max']

        if not am_priority:
            return 0
        return am_priority

    @classmethod
    def get_max_listing_priority(cls):
        listing_priority = cls.objects.filter(
            is_active=True).aggregate(Max('listing_priority'))['listing_priority__max']

        if not listing_priority:
            return 0
        return listing_priority

    @property
    def submission_date(self):
        return datetime.fromtimestamp(self.created_on).strftime('%b %d, %Y')

    @property
    def studio_image_path(self):
        print self.studio_images
        if self.studio_images is None:
            return ''
        path = str(self.studio_images).split('PATH|')
        if len(path) > 1:
            return path[1]
        else:
            return '<a href="/media/{}" >Download</a>'.format(self.studio_images)

    def get_listing_status_history(self):
        status_history = []
        for i in self.listing_status_history:
            bind = dict()
            bind['status'] = i['status']
            bind['updated_by'] = XUser.objects.get(pk=i['updated_by'])
            bind['updated'] = convert_timestamp_to_date(i['timestamp'])
            if i.get('remark'):
                bind['remark'] = i['remark']
            status_history.append(bind)

        return status_history
