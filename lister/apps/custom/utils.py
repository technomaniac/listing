import os
import random
import string
from datetime import datetime

from django.conf import settings

smtp_username = os.environ.get('smtp_username', 'reporting@exclusively.com')
smtp_password = os.environ.get('smtp_password', 'snapdeal@lakhan')
smtp_server = os.environ.get('smtp_server', 'smtp.gmail.com:587')


def get_random_string(length):
    return ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(length))


def handle_uploaded_file(file, dir, name):
    if not os.path.exists(settings.MEDIA_ROOT + dir):
        os.makedirs(settings.MEDIA_ROOT + dir)

    with open(settings.MEDIA_ROOT + dir + name, 'wb+') as destination:
        for chunk in file.chunks():
            destination.write(chunk)

    return dir + name


def get_request_params(queryDict):
    return dict(queryDict.iterlists())


# def send_email(to, subject, msg):
#     if type(to) is str:
#         to = [to]
#     message = """\From: %s\nTo: %s\nSubject: %s\n\n%s
#     """ % (smtp_username, ", ".join(to), subject, msg)
#
#     server = smtplib.SMTP(smtp_server)
#     server.starttls()
#     server.login(smtp_username, smtp_password)
#     server.sendmail(smtp_username, to, message)
#     server.quit()
#
#
# def send_mail_attachment(send_to, subject, text, files=None):
#     assert isinstance(send_to, list)
#
#     msg = MIMEMultipart(
#         From=smtp_username,
#         To=COMMASPACE.join(send_to),
#         Date=formatdate(localtime=True),
#         Subject=subject
#     )
#     msg.add_header('Subject', subject)
#     msg.attach(MIMEText(text))
#
#     for f in files or []:
#         with open(f, "rb") as fil:
#             msg.attach(MIMEApplication(
#                 fil.read(),
#                 Content_Disposition='attachment; filename="%s"' % basename(f),
#                 Name=basename(f)
#             ))
#
#     server = smtplib.SMTP(smtp_server)
#     server.starttls()
#     server.login(smtp_username, smtp_password)
#     server.sendmail(smtp_server, send_to, msg.as_string())
#     server.close()


def convert_timestamp_to_date(timestamp):
    return datetime.fromtimestamp(timestamp).strftime(settings.DATE_FORMAT)
