from django.contrib import admin

# Register your models here.
from .models import *


class SDSellerAdmin(admin.ModelAdmin):
    list_display = ['name', 'code']
    search_fields = ['name', 'code']
    list_filter = ['is_vtipl', 'is_active']


class ExSellerAdmin(admin.ModelAdmin):
    list_display = ['name', 'code']
    search_fields = ['name', 'code']
    # list_filter = ['is_vtipl', 'is_Active']


admin.site.register(SDSeller, SDSellerAdmin)
admin.site.register(ExSeller, ExSellerAdmin)
