from __future__ import unicode_literals

from django.db import models

from ..base.models import BaseModel


class SDSeller(BaseModel):
    # user = models.ForeignKey('xauth.XUser', related_name='sd_seller')
    name = models.CharField(max_length=100, null=True, blank=True)
    code = models.CharField(max_length=50, unique=True)
    is_vtipl = models.BooleanField(default=False)

    def __unicode__(self):
        return '{}'.format(self.name)


class ExSeller(BaseModel):
    # user = models.ForeignKey('xauth.XUser', related_name='ex_seller')
    name = models.CharField(max_length=100, null=True, blank=True)
    code = models.CharField(max_length=50, unique=True)

    def __unicode__(self):
        return '{}'.format(self.name)
