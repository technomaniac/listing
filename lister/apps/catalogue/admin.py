from django.contrib import admin

from .models import *

admin.site.register(Category)
admin.site.register(Bucket)
admin.site.register(Filter)
admin.site.register(Product)
