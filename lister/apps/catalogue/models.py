from __future__ import unicode_literals

from django.contrib.postgres.fields.array import ArrayField
from django.db import models


class Category(models.Model):
    name = models.CharField(max_length=100)
    parent = models.ForeignKey('self', null=True, blank=True)
    level = models.IntegerField(default=1)

    class Meta:
        unique_together = ('name', 'parent', 'level')


class Bucket(models.Model):
    bucket_id = models.IntegerField(unique=True, primary_key=True)
    sub_category = models.ForeignKey('catalogue.Category')
    name = models.CharField(max_length=100, unique=True)
    label_url = models.CharField(max_length=100, unique=True)
    label_name = models.CharField(max_length=100, unique=True)

    def __unicode__(self):
        return '{}'.format(self.name)


class Filter(models.Model):
    FILTER_TYPE = (
        ('Mandatory', 'Mandatory'),
        ('Optional', 'Optional')
    )
    bucket = models.ForeignKey('catalogue.Bucket')
    name = models.CharField(max_length=100)
    type = models.CharField(choices=FILTER_TYPE, max_length=20)
    values = ArrayField(models.CharField(max_length=100), blank=True, null=True)

    class Meta:
        unique_together = ('bucket', 'name')


class Brand(models.Model):
    name = models.CharField(max_length=200, unique=True)


class Product(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField()
