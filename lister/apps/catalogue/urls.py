from django.conf.urls import patterns, url

from .views import *

urlpatterns = patterns('apps.catalogue.views',
                       # url(r'^$', ListingView.as_view()),
                       url(r'^buckets/upload/$', UploadBucketView.as_view()),
                       url(r'^sellers/upload/$', SellerUploadView.as_view()),
                       )
