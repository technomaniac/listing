from django.conf import settings
from django.shortcuts import render

from rest_framework.permissions import IsAdminUser
from rest_framework.views import APIView

from ..custom.utils import handle_uploaded_file
from .tasks import process_bucket_file_task, process_seller_file_task


class UploadBucketView(APIView):
    permission_classes = (IsAdminUser,)
    template_name = 'catalogue/upload_bucket_file.html'

    def get(self, request):
        return render(request, template_name=self.template_name)

    def post(self, request):
        if request.FILES:
            upload_path = 'uploads/buckets/'
            file_path = handle_uploaded_file(request.FILES['bucket_file'], upload_path,
                                             request.FILES['bucket_file'].name)

            process_bucket_file_task(file_path)

            return render(request, template_name=self.template_name, context={'message': 'File is being processed ...'})


class SellerUploadView(APIView):
    permission_classes = (IsAdminUser,)
    template_name = 'catalogue/seller_upload.html'

    def get(self, request):
        return render(request, template_name=self.template_name)

    def post(self, request):
        if request.FILES:
            upload_path = 'uploads/sellers/'
            file_path = handle_uploaded_file(request.FILES['file'], upload_path, request.FILES['file'].name)

            process_seller_file_task(settings.MEDIA_ROOT + file_path, request.POST['seller_type'])

            return render(request, template_name=self.template_name, context={'message': 'File is being processed ...'})
