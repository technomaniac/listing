import os

import mysql.connector
from celery.task.base import task
from django.db.transaction import atomic

from .models import Category, Bucket, Filter
from ..custom.importer import BaseImporter
from ..inventory.models import SDSeller, ExSeller

mysql_config = {'user': 'Cr0nqueue',
                'password': 'R0ng5u$yh7u',
                'host': os.environ.get('MYSQL_HOST', '52.76.57.253'),
                'database': 'analytics',
                'buffered': True}


def get_mysql_connection():
    cnx = mysql.connector.connect(**mysql_config)
    return cnx


@task
@atomic
def process_seller_file_task(seller_file, seller_type='SD'):
    importer = BaseImporter(seller_file)
    header = importer.get_header()
    VTIPL = ['S0b2a2', 'S94b8d', 'S364ff']

    for row in importer.get_rows():
        bind = dict()
        bind['name'] = row[header.index('vendor_name')]
        code = row[header.index('vendor_code')]
        if seller_type == 'SD':
            if code in VTIPL:
                bind['is_vtipl'] = True
            else:
                bind['is_vtipl'] = False

            SDSeller.objects.update_or_create(code=code, defaults=bind)
        else:
            ExSeller.objects.update_or_create(code=code, defaults=bind)


@task
@atomic
def create_ex_sellers():
    sql = '''select DISTINCT vendor_name,vendor_code from exin_community.vendor_marketplace where approved = 1;'''
    cnx = get_mysql_connection()
    cursor = cnx.cursor()
    cursor.execute(sql)
    rows = cursor.fetchall()
    insert_list = []
    unique_list = []
    for row in rows:
        if row[1] not in unique_list:
            insert_list.append(ExSeller(name=row[0], code=row[1]))
            unique_list.append(row[1])

    ExSeller.objects.bulk_create(insert_list)


@task
@atomic
def process_bucket_file_task(bucket_file):
    importer = BaseImporter(bucket_file)
    header = importer.get_header()

    for row in importer.get_rows():
        super_cat, created = Category.objects.get_or_create(name=row[header.index('super category')],
                                                            level=0)

        part_cat, created = Category.objects.get_or_create(name=row[header.index('part category')],
                                                           parent=super_cat,
                                                           level=1)

        cat, created = Category.objects.get_or_create(name=row[header.index('category')],
                                                      parent=part_cat,
                                                      level=2)

        sub_cat, created = Category.objects.get_or_create(name=row[header.index('sub category')],
                                                          parent=cat,
                                                          level=3)

        bucket, created = Bucket.objects.get_or_create(sub_category=sub_cat,
                                                       bucket_id=row[header.index('bucket id')],
                                                       name=row[header.index('bucket name')],
                                                       label_url=row[header.index('label url')],
                                                       label_name=row[header.index('label name')])

        filter_values = [i for i in row[header.index('type') + 1:] if i != '']
        filter, created = Filter.objects.get_or_create(bucket=bucket,
                                                       name=row[header.index('filter')],
                                                       type=row[header.index('type')],
                                                       values=filter_values)
