from django.contrib.auth.hashers import make_password
from django.db.transaction import atomic
from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from rest_framework.permissions import IsAdminUser
from rest_framework.views import APIView

from .models import *
from .tasks import send_email
from ..custom.mixins import LoginRequiredMixin
from ..custom.utils import get_random_string


class AccountListView(LoginRequiredMixin, APIView):
    permission_classes = (IsAdminUser,)

    template_name = 'account/users.html'

    def get(self, request):
        context = dict()
        return render(request, self.template_name, context=context)


class AccountCreateView(LoginRequiredMixin, APIView):
    permission_classes = (IsAdminUser,)

    template_name = 'account/create_user.html'

    def get(self, request):
        context = dict()
        return render(request, self.template_name, context=context)

    @atomic
    def post(self, request):
        email = request.POST['email']
        raw_password = get_random_string(8)
        role = request.POST['role']
        name = request.POST['name']
        phone = request.POST['phone']
        is_admin = False
        password = make_password(raw_password)

        if role == 'admin':
            is_admin = True

        user = XUser.objects.create(username=email, password=password, role=role, is_admin=is_admin)
        ROLE_MODELS[role].objects.create(user=user, name=name, phone=phone)

        message = 'Welcome ! {}\n\n'.format(name)
        message += 'Your Login to your exclusively listing account(Link - {}) using following details - \n'.format(
            'http://listing.exclusively.net/')
        message += 'Username - {}\n'.format(email)
        message += 'Password - {}\n'.format(raw_password)

        try:
            send_email.delay(email, 'Exclusively Listing Tool Login Credentials', message)
        except:
            pass

        return HttpResponseRedirect('/account/')
