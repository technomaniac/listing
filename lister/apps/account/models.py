from __future__ import unicode_literals

from django.contrib.postgres.fields.array import ArrayField
from django.db import models

from ..base.models import BaseModel
from ..xauth.models import XUser


class Admin(BaseModel):
    user = models.OneToOneField('xauth.XUser', related_name='admin')
    name = models.CharField(max_length=100)
    phone = models.CharField(max_length=15, unique=True)
    supervisor = models.ForeignKey('self', null=True, blank=True)
    permissions = ArrayField(models.CharField(max_length=30), null=True, blank=True)

    def __unicode__(self):
        return self.name

    def save(self, *args, **kwargs):
        created = False
        if not self.pk:
            created = True
        super(Admin, self).save(*args, **kwargs)
        if created:
            self.update_user_role(self.user.pk)

    @staticmethod
    def update_user_role(pk):
        XUser.objects.filter(pk=pk, role=None).update(role='admin')


class ListingOperations(BaseModel):
    user = models.OneToOneField('xauth.XUser', related_name='listing_operations')
    name = models.CharField(max_length=100)
    phone = models.CharField(max_length=15, unique=True)
    supervisor = models.ForeignKey('self', null=True, blank=True)
    permissions = ArrayField(models.CharField(max_length=30), null=True, blank=True)

    def __unicode__(self):
        return self.name

    def save(self, *args, **kwargs):
        created = False
        if not self.pk:
            created = True
        super(ListingOperations, self).save(*args, **kwargs)
        if created:
            self.update_user_role(self.user.pk)

    @staticmethod
    def update_user_role(pk):
        XUser.objects.filter(pk=pk, role=None).update(role='listing_operations')


class ListingContent(BaseModel):
    user = models.OneToOneField('xauth.XUser', related_name='listing_content')
    name = models.CharField(max_length=100)
    phone = models.CharField(max_length=15, unique=True)
    supervisor = models.ForeignKey('self', null=True, blank=True)
    permissions = ArrayField(models.CharField(max_length=30), null=True, blank=True)

    def __unicode__(self):
        return self.name

    def save(self, *args, **kwargs):
        created = False
        if not self.pk:
            created = True
        super(ListingContent, self).save(*args, **kwargs)
        if created:
            self.update_user_role(self.user.pk)

    @staticmethod
    def update_user_role(pk):
        XUser.objects.filter(pk=pk, role=None).update(role='listing_content')


class StudioManager(BaseModel):
    user = models.OneToOneField('xauth.XUser', related_name='studio_manager')
    name = models.CharField(max_length=100)
    phone = models.CharField(max_length=15, unique=True)
    supervisor = models.ForeignKey('self', null=True, blank=True)
    permissions = ArrayField(models.CharField(max_length=30), null=True, blank=True)

    def __unicode__(self):
        return self.name

    def save(self, *args, **kwargs):
        created = False
        if not self.pk:
            created = True
        super(StudioManager, self).save(*args, **kwargs)
        if created:
            self.update_user_role(self.user.pk)

    @staticmethod
    def update_user_role(pk):
        XUser.objects.filter(pk=pk, role=None).update(role='studio_manager')



class AccountManager(BaseModel):
    user = models.OneToOneField('xauth.XUser', related_name='account_manager')
    name = models.CharField(max_length=100)
    phone = models.CharField(max_length=15, unique=True)
    supervisor = models.ForeignKey('self', null=True, blank=True)
    permissions = ArrayField(models.CharField(max_length=30), null=True, blank=True)

    def __unicode__(self):
        return self.name

    def save(self, *args, **kwargs):
        created = False
        if not self.pk:
            created = True
        super(AccountManager, self).save(*args, **kwargs)
        if created:
            self.update_user_role(self.user.pk)

    @staticmethod
    def update_user_role(pk):
        XUser.objects.filter(pk=pk, role=None).update(role='account_manager')


class QC(BaseModel):
    user = models.OneToOneField('xauth.XUser', related_name='qc')
    name = models.CharField(max_length=100)
    phone = models.CharField(max_length=15, unique=True)
    supervisor = models.ForeignKey('self', null=True, blank=True)
    permissions = ArrayField(models.CharField(max_length=30), null=True, blank=True)

    def __unicode__(self):
        return self.name

    def save(self, *args, **kwargs):
        created = False
        if not self.pk:
            created = True
        super(QC, self).save(*args, **kwargs)
        if created:
            self.update_user_role(self.user.pk)

    @staticmethod
    def update_user_role(pk):
        XUser.objects.filter(pk=pk, role=None).update(role='qc')


ROLE_MODELS = {'admin': Admin,
               'account_manager': AccountManager,
               'listing_content': ListingContent,
               'listing_operations': ListingOperations,
               'studio_manager': StudioManager}
