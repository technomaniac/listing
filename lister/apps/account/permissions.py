from rest_framework.permissions import BasePermission


class IsAccountManager(BasePermission):
    def has_permission(self, request, view):
        try:
            if request.user.account_manager:
                return True
            else:
                return False
        except:
            return False


class IsStudioManager(BasePermission):
    def has_permission(self, request, view):
        try:
            if request.user.studio_manager:
                return True
            else:
                return False
        except:
            return False


class IsListingManager(BasePermission):
    def has_permission(self, request, view):
        try:
            if request.user.listing_manager:
                return True
            else:
                return False
        except:
            return False
