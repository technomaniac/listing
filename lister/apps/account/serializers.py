from rest_framework import serializers

from .models import ListingContent


class ListingContentSerializer(serializers.ModelSerializer):
    # action = serializers.ReadOnlyField(source='id', read_only=True)

    class Meta:
        model = ListingContent
        fields = ('id', 'name')
