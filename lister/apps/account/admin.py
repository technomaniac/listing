from django.contrib import admin

# Register your models here.
from .models import *

admin.site.register(AccountManager)
admin.site.register(QC)
admin.site.register(StudioManager)
admin.site.register(ListingOperations)
admin.site.register(ListingContent)
admin.site.register(Admin)