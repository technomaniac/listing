import os
import smtplib
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import COMMASPACE, formatdate
from os.path import basename

from celery.task.base import task

smtp_username = os.environ.get('smtp_username', 'reporting@exclusively.com')
smtp_password = os.environ.get('smtp_password', 'snapdeal@lakhan')
smtp_server = os.environ.get('smtp_server', 'smtp.gmail.com:587')


@task
def send_email(to, subject, msg):
    if type(to) is str:
        to = [to]
    message = """\From: %s\nTo: %s\nSubject: %s\n\n%s
    """ % (smtp_username, ", ".join(to), subject, msg)

    server = smtplib.SMTP(smtp_server)
    server.starttls()
    server.login(smtp_username, smtp_password)
    server.sendmail(smtp_username, to, message)
    server.quit()


@task
def send_mail_attachment(send_to, subject, text, files=None):
    assert isinstance(send_to, list)

    msg = MIMEMultipart(
        From=smtp_username,
        To=COMMASPACE.join(send_to),
        Date=formatdate(localtime=True),
        Subject=subject
    )
    msg.add_header('Subject', subject)
    msg.attach(MIMEText(text))

    for f in files or []:
        with open(f, "rb") as fil:
            msg.attach(MIMEApplication(
                fil.read(),
                Content_Disposition='attachment; filename="%s"' % basename(f),
                Name=basename(f)
            ))

    server = smtplib.SMTP(smtp_server)
    server.starttls()
    server.login(smtp_username, smtp_password)
    server.sendmail(smtp_server, send_to, msg.as_string())
    server.close()
