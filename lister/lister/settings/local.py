import os

DEBUG = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': os.environ.get('DB_NAME', 'listing'),
        'USER': os.environ.get('DB_USER', 'abhishek'),
        'PASSWORD': os.environ.get('DB_PASS', '1'),
        'HOST': os.environ.get('DB_HOST', '127.0.0.1'),
        'PORT': '5432',
    }
}

# from pymongo.mongo_client import MongoClient
#
# MONGO_CLIENT = MongoClient(host=os.environ.get('MONGODB_HOST', 'localhost'),
#                            port=int(os.environ.get('MONGODB_PORT', 27017)))
#
# XIPPER_DB = MONGO_CLIENT["xipper"]

import djcelery

djcelery.setup_loader()

BROKER_URL = 'amqp://'
CELERY_RESULT_BACKEND = 'djcelery.backends.database.DatabaseBackend'
CELERYBEAT_SCHEDULER = 'djcelery.schedulers.DatabaseScheduler'

