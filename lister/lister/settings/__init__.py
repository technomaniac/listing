from .base import *

deployment_type = os.environ.get("DEPLOYMENT_TYPE")

if deployment_type:
    if deployment_type == 'STAGING':
        from .stag import *
    elif deployment_type == 'PRODUCTION':
        from .prod import *
else:
    from .local import *
