import os

DEBUG = False

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': os.environ.get('DB_NAME', 'listing'),
        'USER': os.environ.get('DB_USER', 'listing'),
        'PASSWORD': os.environ.get('DB_PASS', 'listing123'),
        'HOST': os.environ.get('DB_HOST', '127.0.0.1'),
        'PORT': '5432',
    }
}

# from pymongo.mongo_client import MongoClient
#
# MONGO_CLIENT = MongoClient(host=os.environ.get('MONGODB_HOST', 'localhost'),
#                            port=int(os.environ.get('MONGODB_PORT', 27017)))
#
# XIPPER_DB = MONGO_CLIENT["xipper"]

