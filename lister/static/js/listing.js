$.ajaxSetup({
    beforeSend: function (xhr, settings) {
        function getCookie(name) {
            var cookieValue = null;
            if (document.cookie && document.cookie != '') {
                var cookies = document.cookie.split(';');
                for (var i = 0; i < cookies.length; i++) {
                    var cookie = jQuery.trim(cookies[i]);
                    if (cookie.substring(0, name.length + 1) == (name + '=')) {
                        cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                        break;
                    }
                }
            }
            return cookieValue;
        }

        if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
            xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
        }
    }
});

LISTING_STATUS = ['Yet to Start', 'Inward', 'QC Done', 'Shoot Done', 'Foldering Done', 'Editing Done', 'Content Sheet Ready', 'Upload Format Ready', 'Upload Done'];
STUDIO_LISTING_STATUS = ['Yet to Start', 'Inward', 'QC Done', 'Shoot Done', 'Foldering Done', 'Editing Done'];
LISTING_CONTENT_STATUS = ['Editing Done', 'Content Sheet Ready'];
LISTING_OPS_STATUS = ['Upload Format Ready', 'Upload Done'];

LISTING_STATUS_CSS = {
    'Yet to Start': 'info',
    'Inward': 'primary',
    'Shoot Done': 'warning',
    'Foldering Done': 'default',
    'Editing Done': 'danger',
    'Content Sheet Ready': 'info',
    'Upload Format Ready': 'primary',
    'Upload Done': 'success'
};

function set_mask(box) {
    var id = '#popup';
    var mask = $('#mask');
    //Get the screen height and width
    var maskHeight = $(window).height();
    var maskWidth = $(window).width();
    //Set heigth and width to mask to fill up the whole screen
    mask.css({'width': maskWidth, 'height': maskHeight});
    //transition effect
    mask.fadeIn(500);
    mask.fadeTo("slow", 0.6);
    //Get the window height and width
    var winH = $(window).height();
    var winW = $(window).width();
    //Set the popup window to center
    $(id + ' #dialog').html(box);

    $(id + ' #dialog').css('top', winH / 2 - $(id + ' #dialog').height() / 2 - 50);
    $(id + ' #dialog').css('left', winW / 2 - $(id + ' #dialog').width() / 2);
    //transition effect
    $(id).fadeIn(2000);
    //if close button is clicked
    $('.window .close-box').click(function (e) {
        //Cancel the link behavior
        e.preventDefault();

        mask.hide(500);
        $(id + ' #dialog').html("");
        $(id + ' #dialog').attr('style', '');
        mask.attr('style', '');
    });
    //if mask is clicked
    mask.click(function () {
        $(this).hide(500);
        $(id + ' #dialog').html("");
        $(id + ' #dialog').attr('style', '');
        mask.attr('style', '');
    });
}

function update_listing(id, data) {
    $.ajax({
        type: 'post',
        url: '/listing/' + id + '/',
        data: data,
        success: function (response) {
            $('#listing').DataTable().ajax.reload();
        }
    });
}


function listing_action(id, action) {
    $.ajax({
        type: 'get',
        url: '/listing/' + id + '/action/',
        data: {
            'action': action
        },
        success: function (response) {
            if (action == 'revert') {
                $('#listing').DataTable().ajax.reload();
            } else {
                set_mask(response);
            }
        }
    });
}

function update_status(ele, id) {
    var action = $(ele).val();
    if (action == 'Inward' || action == 'Editing Done' || action == 'Content Sheet Ready' || action == 'Upload Format Ready') {
        $.ajax({
            type: 'get',
            url: '/listing/' + id + '/action/',
            data: {
                'action': action
            },
            success: function (response) {
                set_mask(response);
            }
        });
    }
}

setInterval(function () {
    $('#listing').DataTable().ajax.reload();
}, 5 * 60 * 1000);